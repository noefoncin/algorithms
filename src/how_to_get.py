from search_in_db import *
import json

f = open("regions.json")
data = json.load(f)


def zone():
    while True:
        urbanization_level = input(
            "Souhaitez-vous réaliser vos semis en zone urbaine, ou rurale ? Saisissez [URBAIN] ou [RURAL]")
        if urbanization_level == "URBAIN":
            print("Si vous habitez en zone urbaine, soyez vigilants quant à l'emplacement de vos semis :\nÀ une distance inférieure à 10 mètres d'un axe sur lequel circulent plus de 5000 véhicules par jour,\n les légumes ou fruits que vous pourrez cultiver ne seront plus propres à la consommation. ")
            break
        elif urbanization_level == "RURAL":
            print("plant_localization")
            break


def gardenToxicity():
    while True:
        plant_localization = input(
            "Où seront installés les semis ? Saisissez [POTAGER], [BALCON] ou [AUTRE] ")
        if plant_localization == "POTAGER":
            print(plant_localization)
            break
        elif plant_localization == "BALCON":
            print("plant_localization")
            break
        elif plant_localization == "AUTRE":
            print("plant_localization")
            break


def howToGet():
    print("Vous avez sélectionné la partie HOWTOGET de l'algorithme.")
    wanted_plant = input(
        "Quelle est la plante que vous cherchez à faire pousser?")
    search_plant_db(wanted_plant)
    plant_localization = gardenToxicity()
    sow_zone = zone()
    while True:
        departement = input("Dans quel département résidez-vous ?")
        localisation_infos = list(
            filter(lambda x: x["num_dep"] == str(departement), data))
        if localisation_infos != []:
            break
    while True:
        sow_time = input(
            "Quelle est la période à laquelle vous voudriez réaliser vos semis?\n Saisissez [PRINTEMPS],[ETE],[AUTOMNE] ou [HIVER]")
        if sow_time == "PRINTEMPS":
            verify_conditions("spring", wanted_plant, localisation_infos)
            break
        if sow_time == "ETE":
            verify_conditions("summer", wanted_plant, localisation_infos)
            break
        if sow_time == "AUTOMNE":
            print("La plupart des semis ne se réalisent pas durant la période automnale.\n Vous pouvez toutefois vous renseigner au sujet des potagers d'intérieur, qui eux ne sont pas dépendants des intempéries.")
            verify_conditions("fall", wanted_plant, localisation_infos)
            break
        if sow_time == "HIVER":
            print("La plupart des semis ne se réalisent pas durant la période hivernale.\n Vous pouvez toutefois vous renseigner au sujet des potagers d'intérieur, qui eux ne sont pas dépendants des intempéries.")
            verify_conditions("winter", wanted_plant, localisation_infos)
            break
